package com.digital.strategy.followup;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.support.v7.widget.CardView;
import android.transition.Transition;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageButton;

/**
 * Created by tech on 9/25/15.
 */
public class PageAnimation extends Activity {

    private CustomViewPager viewPager ;
    private CardView cardview ;

    public boolean cardAnimator (final CardView cardView, int minHeight, int maxHeight){
        ValueAnimator anim;
        boolean isCollapsed;

        if (cardView.getHeight() == minHeight) {
            // collapse
            anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                    maxHeight);


            isCollapsed = true;
        } else {
            // expand
            anim = ValueAnimator.ofInt(cardView.getMeasuredHeightAndState(),
                    minHeight);

            isCollapsed = false;
        }

        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = cardView.getLayoutParams();
                layoutParams.height = val;
                cardView.setLayoutParams(layoutParams);
            }
        });
        anim.start();

        return isCollapsed;
    }

    public void buttonAnimator (final ImageButton button, boolean card_state_collapsed){

        if(card_state_collapsed == true){
            //Card is collapsed
            AlphaAnimation fade_out = new AlphaAnimation(1.0f, 0.0f);
            fade_out.setDuration(500);
            fade_out.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation arg0) {
                    button.setVisibility(View.GONE);
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationEnd(Animation arg0) {

                }
            });
            button.startAnimation(fade_out);
        }
        else{
            AlphaAnimation fade_in = new AlphaAnimation(0.0f, 1.0f);
            fade_in.setDuration(500);
            fade_in.setAnimationListener(new Animation.AnimationListener() {
                public void onAnimationStart(Animation arg0) {
                    button.setVisibility(View.VISIBLE);
                }

                public void onAnimationRepeat(Animation arg0) {
                }

                public void onAnimationEnd(Animation arg0) {

                }
            });
            button.startAnimation(fade_in);
        }
    }

    public void setViewPager (final CustomViewPager viewPager){
        this.viewPager = viewPager;
    }

    public CustomViewPager getViewPager (){
        return viewPager;
    }

    public void setCardView (final CardView cardview){
        this.cardview = cardview;
    }

    public CardView getCardView (){
        return cardview;
    }

    public void enterReveal(final View button, final Transition.TransitionListener mEnterTransitionListener) {

        // get the center for the clipping circle
        int cx = button.getMeasuredWidth() / 2;
        int cy = button.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(button.getWidth(), button.getHeight()) / 2;

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(button, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        button.setVisibility(View.VISIBLE);
        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                //getWindow().getEnterTransition().removeListener(mEnterTransitionListener);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        anim.start();
    }

    public void exitReveal(final View button) {

        // get the center for the clipping circle
        int cx = button.getMeasuredWidth() / 2;
        int cy = button.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = button.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(button, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                button.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }
}
